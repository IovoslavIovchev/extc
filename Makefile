build: build_dir lex parse
	gcc build/extc.tab.c build/extc.lex.c src/main.c src/ast.c src/aux.c -I./ -o build/ec

run: build
	./build/ec

build_dir:
	mkdir -p build

clean:
	rm -rf build

parse: build_dir src/extc.y
	bison --defines=build/extc.tab.h -o build/extc.tab.c ./src/extc.y

lex: build_dir parse src/extc.l
	flex -o build/extc.lex.c ./src/extc.l
