%{

    #include <stdlib.h>
    #include <string.h>

    #include "build/extc.tab.h"

%}

%option yylineno noyywrap reentrant bison-bridge
%option header-file="build/extc.lex.h"

%%

[0-9]+ {
    yylval_param->int_val = atoi(yytext);
    return INT_LIT;
}

\"(\\.|[^\"])*\" {
    yylval_param->str_val = yytext;
    return STRING_LIT;
}

'.' {
    yylval_param->char_val = yytext[1];
    return CHAR_LIT;
}

; { return TERM; }
:: { return SCOPE; }

= { return ASSIGNMENT_OP; }

const { return CONST_KW; }

[a-zA-z_][a-zA-z_0-9]* {
    yylval_param->str_val = yytext;
    return IDENT;
}

[ \t\n] ;

%%
