%{

    #include "build/extc.tab.h"
    #include "build/extc.lex.h"

    int yyerror(void* scanner, const char *msg);
%}

%define api.pure full
%define parse.error verbose

%lex-param { void* scanner }
%parse-param { void* scanner }

%code requires {
    #include <stdint.h>
    #include "src/ast.h"
}

%union {
    char char_val;
    uint64_t int_val;
    double float_val;
    char* str_val;

    List list_val;
    AstIdent ident_val;
    Type type_val;
    Literal lit;
    Expr* expr_ptr;
    Node* node_ptr;
}

%token TERM SCOPE
%token TYPE_CONST ASSIGNMENT_OP
%token CONST_KW
%token <str_val> IDENT

%token <char_val> CHAR_LIT
%token <int_val> INT_LIT
%token <float_val> FLOAT_LIT
%token <str_val> STRING_LIT

%type <node_ptr> top
%type <node_ptr> module
%type <node_ptr> node
%type <node_ptr> const
%type <expr_ptr> expr
%type <type_val> type
%type <list_val> path
%type <ident_val> ident
%type <lit> lit

%left '-' '+'
%left '*' '/'

%start top

%%

top: module { AST_ROOT = $1; }
   ;

module: node { $$ = make_module(make_ast_ident("main"), $1); }
      | module node { append_to_module($$, $2); }
      ;

node: const
    ;

const: CONST_KW type ident ASSIGNMENT_OP expr TERM { $$ = make_const($3, $2, $5); }
     ;

statement: const
         ;

expr: lit { $$ = make_lit($1); }
    | path { $$ = make_path_expr($1); }
    ;

type: path { $$ = make_type($1); }
    /* TODO: | path '<' path '>'*/
    ;

path: ident { $$ = make_singleton(&$1); }
    | path SCOPE ident { append_to_list(&$$, &$3); }
    ;

ident: IDENT { $$ = make_ast_ident($1); }
     ;

lit: INT_LIT { $$ = make_int($1); }
   ;

%%

int yyerror(void* scanner, const char *msg)
{
    printf("%s", msg);
    AST_ROOT = NULL;
    return 1;
}

