#include <stdlib.h>

#include "src/aux.h"

List make_list()
{
    List l;
    l.first = NULL;
    l.last = NULL;

    return l;
}

List make_singleton(void* elem)
{
    List l = make_list();
    append_to_list(&l, elem);

    return l;
}

void append_to_list(List* list, void* elem)
{
    ListNode* new = malloc(sizeof(ListNode));
    new->data = elem;
    new->next = NULL;

    if (list->first == NULL)
    {
        list->first = new;
        list->last = new;
    }
    else
    {
        list->last->next = new;
        list->last = new;
    }
}

void free_list(List* list, void (*free)(void*))
{
    ListNode* node = list->first;
    while (node != NULL)
    {
        free(node);
        node = node->next;
    }
}
