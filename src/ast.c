#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "src/ast.h"

void print_ast_ident(const AstIdent ident)
{
    for (uint64_t i = 0; i < ident.len; ++i)
        printf("%c", ident.str[i]);
}

void print_expr_impl(const Expr* e, uint64_t indent)
{
    switch (e->kind)
    {
    case EXPR_KIND_LIT:
        printf("%ld", e->lit.uint);
        break;

    case EXPR_KIND_PATH:
        printf("a path");
        break;

    default:
        break;
    }
}

void print_ast_impl(const Node* ast, uint64_t indent)
{
    switch (ast->kind)
    {
    case NODE_KIND_MOD:
        printf("Module ");
        print_ast_ident(ast->ident);
        printf("\n\n");

        FOREACH(item, ast->module.items)
        {
            print_ast_impl(item->data, indent);
            item = item->next;
        }

        break;

    case NODE_KIND_CONST:
        printf("const ");
        print_ast_ident(ast->ident);
        printf(" = ");
        print_expr_impl(ast->constant.val, indent);
        printf("\n");
        break;

    default:
        break;
    }
}

void print_ast(const Node* ast)
{
    assert(ast != NULL);
    print_ast_impl(ast, 0);
}

static inline Expr* init_expr()
{
    return (Expr*)malloc(sizeof(Expr));
}

static inline Node* init_node()
{
    return (Node*)malloc(sizeof(Node));
}

Literal make_int(const uint64_t i)
{
    Literal l;
    l.kind = LIT_KIND_UNSIGNED_INT;
    l.uint = i;

    return l;
}

Expr* make_lit(Literal lit)
{
    Expr* e = init_expr();
    e->kind = EXPR_KIND_LIT;
    e->lit = lit;

    return e;
}

Expr* make_path_expr(List path)
{
    Expr* e = init_expr();
    e->kind = EXPR_KIND_PATH;
    e->path = path;

    return e;
}

Type make_type(List path)
{
    Type t;
    t.kind = TY_KIND_PATH;
    t.path = path;

    return t;
}

Node* make_const(AstIdent ident, Type type, Expr* val)
{
    Node* node = init_node();
    node->kind = NODE_KIND_CONST;
    node->ident = ident;
    node->constant.type = type;
    node->constant.val = val;

    return node;
}

Node* make_module(AstIdent ident, Node* first)
{
    Node* node = init_node();
    node->kind = NODE_KIND_MOD;
    node->ident = ident;

    List list = make_list();
    append_to_list(&list, first);

    node->module.items = list;

    return node;
}

void append_to_module(Node* mod, Node* item)
{
    append_to_list(&mod->module.items, item);
}

AstIdent make_ast_ident(char* s)
{
    AstIdent ident;
    ident.len = strlen(s);
    ident.str = s;

    return ident;
}

void free_node(Node* node)
{
    // TODO
}

void free_expr(Expr* e)
{
    // TODO
}
