#include <stdio.h>

#include "build/extc.tab.h"
#include "build/extc.lex.h"

static const int BUFFER = 16384;

int main(int argc, char* argv[])
{
    if (argc < 2) { return 1; }

    yyscan_t scanner = 0;
    yylex_init(&scanner);

    FILE* f = fopen(argv[1], "r");
    YY_BUFFER_STATE buf = yy_create_buffer(f, BUFFER, scanner);
    yy_switch_to_buffer(buf, scanner);

    int parse_res = yyparse(scanner);

    print_ast(AST_ROOT);

    free_node(AST_ROOT);
    yylex_destroy(scanner);

    return parse_res;
}
