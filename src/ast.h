#ifndef EXTC_AST
#define EXTC_AST

#include <stdint.h>

#include "src/aux.h"

typedef struct
{
    char* str;
    uint64_t len;
} AstIdent;

typedef enum
{
    TY_KIND_PATH,
    TY_KIND_POINTER
} TypeKind;

typedef struct type_t
{
    TypeKind kind;

    union {
        List /* AstIdent */ path;

        struct {
            uint8_t is_mut;
            struct type_t* type;
        } pointer;
    };
} Type;

typedef enum
{
    LIT_KIND_UNSIGNED_INT
} LiteralKind;

typedef struct
{
    LiteralKind kind;

    union {
        uint64_t uint;
    };
} Literal;

typedef enum
{
    EXPR_KIND_LIT,
    EXPR_KIND_PATH,
} ExprKind;

typedef struct
{
    ExprKind kind;

    union {
        Literal lit;
        List /* AstIdent */ path;
    };
} Expr;

typedef enum
{
    NODE_KIND_MOD,
    NODE_KIND_CONST
} NodeKind;

typedef struct Node
{
    NodeKind kind;
    AstIdent ident;

    union {
        struct {
            List/* Node */ items;
        } module;

        struct {
            Type type;
            Expr* val;
        } constant;
    };
} Node;

void print_ast(const Node*);
void free_node(Node*);

Node* AST_ROOT;

Literal make_int(const uint64_t);

Expr* make_lit(const Literal);
Expr* make_path_expr(const List);

Type make_type(List);
Node* make_const(AstIdent, Type, Expr*);

Node* make_module(AstIdent, Node*);
void append_to_module(Node*, Node*);

AstIdent make_ast_ident(char*);

#endif
