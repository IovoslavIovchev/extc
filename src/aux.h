#ifndef EXTC_AUX_H
#define EXTC_AUX_H

#define FOREACH(ID, LIST) \
    ListNode* ID = LIST.first; \
    while (ID != 0)

typedef struct node_t
{
    void* data;
    struct node_t* next;
} ListNode;

typedef struct list_t
{
    ListNode* first;
    ListNode* last;
} List;

List make_list();
List make_singleton(void*);
void append_to_list(List*, void*);
void free_list(List*, void (*)(void*));

#endif
